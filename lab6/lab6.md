# Lab6

## 1. You have the task to configure subinterfaces with VLANs using 2 methods

1. by using netplan
2. by using ip link command

Below we provided 2 different VLANs to be configured with both mentioned methods accordingly:

1. vlan 10 with ip addresses 192.168.10.10/24 and its gateway 192.168.10.1
2. vlan 20 with ip addresses 192.168.20.20/24 and its gateway 192.168.20.1

For ip link command manuals you can use this [link](http://manpages.ubuntu.com/manpages/trusty/man8/ip-link.8.html)

For netplan command manuals you can use this [link](https://netplan.io/examples/)

**Proof of successful results and configurations for both methods should be provided in the report.**

## Solution

### Netplan

Let us edit `/etc/netplan/01-network-manager-all.yaml` as following:

    network:
        version: 2
        renderer: networkd
        ethernets:
            enp0s3:
                dhcp4: no
                addresses:
                    - 10.0.2.15/24
                gateway4: 10.0.2.2
                nameservers:
                    addresses: [8.8.8.8, 1.1.1.1]
        vlans:
            vlan10:
                id: 10
                link: enp0s3
                addresses:
                    - 192.168.10.10/24
                routes:
                    - to: default
                      via: 192.168.10.1
                      on-link: true
                      metric: 1
            vlan20:
                id: 20
                link: enp0s3
                addresses:
                    - 192.168.20.20/24
                routes:
                    - to: default
                      via: 192.168.20.1
                      on-link: true
                      metric: 2

Then apply this configuration using `netplan try` and `netplan apply` commands.\
![](images/1.1.1.png)

As result, we have the following:\
![](images/1.1.2.png)\
![](images/1.1.3.png)\
![](images/1.1.4.png)

### ip link

Let us configure sub-interfaces using ip link.\
![](images/1.2.1.png)

As result, we have the following:\
![](images/1.2.2.png)\
![](images/1.2.3.png)\
![](images/1.2.4.png)

## 2. In this task you should create VM under Ubuntu OS with 3 interfaces

1. first interface with adapter NAT
2. second interface with adapter INTERNAL NETWORK and Name=intnet (enabled promiscious mode)
3. third interface with adapter INTERNAL NETWORK and Name=intnet2 (enabled promiscious mode))

Then you need to make a virtual bridge to be able to connect 2 different interfaces (intnet, intnet2). In order to
successfully complete the task, you need to show that now your virtual bridge (switch) has both MAC addresses in its
table.

**Provide proof and important configurations in the report.**

## Solution

Interfaces configured as said in task.\
![](images/2.1.png)\
![](images/2.2.png)\
![](images/2.3.png)

As you can see, interfaces are present, we need to configure them.\
![](images/2.4.png)

Let us configure bridge and connect 2 internal interfaces.\
![](images/2.5.png)

As you can see, MAC addresses of these interfaces present in table.\
![](images/2.6.png)

(C) Abuzyar Tazetdinov
