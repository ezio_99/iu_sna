# Lab8

## 1. Generate ssh key pair with different from in the exercise encryption algorithm. Use those keys to access a remote machine (VM for example). Provide all necessary secure configuration.

Create public-private key pair on host.\
![](images/1.1.png)

Copy public key to VM.\
![](images/1.2.png)

Connect to VM using private key.\
![](images/1.3.png)

Make ssh service more secure. For this purpose add following directives to `/etc/ssh/sshd_config` and restart ssh
service using `sudo service ssh restart` command.

    PasswordAuthentication no
    PermitRootLogin no
    ChallengeResponseAuthentication no
    UsePAM no
    AuthenticationMethods publickey
    PubkeyAuthentication yes
    AllowUsers ezio

Now ssh sessions using password are denied.\
![](images/1.4.png)

## 2. Create root certificate CA and generate your domain certificate with public and private keys. Generate a Certificate Signing Request for your domain certificate then generate your certificate with use the CA’s signature to form a certificate using CA certificate, set expiration days to 365. Show your content of certificate with attributes: ISSUER, Validity, Serial Number, Subject etc. Convert your certificate to DER format (install openssl tool with apt/yum). Keep these certificates, it will be used for the next LAB.

Create root CA key.\
![](images/2.1.png)

Create Certificate Signing Request (CSR) for root CA key.\
![](images/2.2.png)

Sign root CA certificate.\
![](images/2.3.png)

Create key for domain `home.net`.\
![](images/2.4.png)

Create CSR for domain `home.net`.\
![](images/2.5.png)

Sign certificate for `home.net`.\
![](images/2.6.png)

Show content of `home.net` certificate.\
![](images/2.7.png)

Convert certificate for `hone.net` to DER format.\
![](images/2.8.png)

(C) Abuzyar Tazetdinov
