#!/usr/bin/env bash

date_file=$1
df_file=$2

date +"%d.%m.%Y %H:%M:%S" > "$date_file"
df -hT > "$df_file"
