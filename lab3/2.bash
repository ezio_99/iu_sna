#!/usr/bin/env bash

file=$1
if [ -z "$2" ]; then
    mode="create"
else
    mode=$2
fi

if ! echo "$mode" | grep -q -E "(create|delete)"; then
    echo "Mode should be \"create\" or \"delete\""
    exit 1
fi

function create_user() {
    username=$1
    password=$2

    echo -n "Adding $username ... "
    useradd -m -s /bin/bash "$username" 2>/dev/null
    echo "$username:$password" | chpasswd
    echo "Done"
}

function delete_user() {
    username=$1

    echo -n "Removing $username ... "
    userdel -rf "$username" 2>/dev/null
    echo "Done"
}

function get_column() {
    line=$1
    column=$2

    echo "$line" | tr -s " " " " | cut -f "$column" -d " "
}

while read -r line; do
    if [ -z "$line" ]; then
        continue
    fi

    username=$(get_column "$line" 1)
    password=$(get_column "$line" 2)

    if [[ $mode == "delete" ]]; then
        delete_user "$username"
    elif [[ $mode == "create" ]]; then
        create_user "$username" "$password"
    fi
done <"$file"
