#!/usr/bin/env bash

start=1
end=11
step=2

i=$start
while [[ $i -lt $end ]] ;do
    echo $i
    i=$((i + step))
done
