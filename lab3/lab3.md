# Lab3

### 1. Create a simple script to print odd numbers from 1 to 10.

Script can be found in `1.bash`. Also content listed below.

    #!/usr/bin/env bash

    start=1
    end=11
    step=2
    
    i=$start
    while [[ $i -lt $end ]] ;do
        echo $i
        i=$((i + step))
    done

![](images/1.png)

### 2. Create a shell script which makes new users and their corresponding passwords for 10 accounts in the system. Script should read the data from the file users.txt (you can write data to this file).

Script can be found in `2.bash`. Also content listed below.

    #!/usr/bin/env bash

    file=$1
    if [ -z "$2" ]; then
        mode="create"
    else
        mode=$2
    fi
    
    if ! echo "$mode" | grep -q -E "(create|delete)"; then
        echo "Mode should be \"create\" or \"delete\""
        exit 1
    fi
    
    function create_user() {
        username=$1
        password=$2
    
        echo -n "Adding $username ... "
        useradd -m -s /bin/bash "$username" 2>/dev/null
        echo "$username:$password" | chpasswd
        echo "Done"
    }
    
    function delete_user() {
        username=$1
    
        echo -n "Removing $username ... "
        userdel -rf "$username" 2>/dev/null
        echo "Done"
    }
    
    function get_column() {
        line=$1
        column=$2
    
        echo "$line" | tr -s " " " " | cut -f "$column" -d " "
    }
    
    while read -r line; do
        if [ -z "$line" ]; then
            continue
        fi
    
        username=$(get_column "$line" 1)
        password=$(get_column "$line" 2)
    
        if [[ $mode == "delete" ]]; then
            delete_user "$username"
        elif [[ $mode == "create" ]]; then
            create_user "$username" "$password"
        fi
    done <"$file"

File with users should contain 2 columns delimited by space. First column for user, second for password.

Usage:

1. Create users from file

        sudo ./2.bash <file_with_users> create
2. Delete users from file

        sudo ./2.bash <file_with_users> delete

![](images/2.png)

### 3. Provide an example of the shell script where you can pass result (not an exit code) of the executed function in a subshell to the parent’s shell.

Value can be returned from child using `echo` and captured in parent like `$(<run_child>)`

Children script in file `3.bash`. Also content listed below.

    #!/usr/bin/env bash

    echo "Today is $(date +"%d.%m.%Y")"

![](images/3.png)

### 4. Create script that redirects system date and disk utilization in a file with systemd.

Script can be found in `4.bash`. Also content listed below.

    # !/usr/bin/env bash

    date_file=$1
    df_file=$2
      
    date +"%d.%m.%Y %H:%M:%S" > "$date_file"
    df -hT > "$df_file"

Unit file can be found in `date_df.service`. Also content listed below.

    [Unit]
    Description=Date and disk utilization logging into file
    
    [Service]
    ExecStart=/usr/bin/4.bash /root/date.txt /root/df.txt
    
    [Install]
    WantedBy=multi-user.target

1. Copy these files into directories from where they be accessed by systemd. Then enable and start our service.\
   ![](images/4.1.png)
2. Check that our service successfully finished.\
   ![](images/4.2.png)
3. Print content of files filled by our service.\
   ![](images/4.3.png)

(C) Abuzyar Tazetdinov
