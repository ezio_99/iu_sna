# Lab7

## 1. Set your interface on VM as bridged connection and restart interface (interface with network adapter BRIDGED ADAPTER). Install iptables with apt (if you don't have it). Check existing rules and display them.

![](images/1.1.png)\
![](images/1.2.png)\
![](images/1.3.png)

## 2. Working with INPUT and OUTPUT chains.

To test each of the above rules, try to ping VM and observe whether it is successful or not.

Record your rules in the report, its short explanation and results of the implementations (acceptance testings)

### add a rule1 that blocks ping packets coming into the VM (your server with installed iptables).

    sudo iptables -A INPUT -p icmp --icmp-type echo-request -j REJECT
    # -A INPUT -- add rule to INPUT chain
    # -p icmp -- match ICMP protocol
    # --icmp-type echo-request -- match ICMP echo requests
    # -j REJECT -- reject matched packets

![](images/2.1.1.png)\
![](images/2.1.2.png)

### delete the rule1, and then add rule2 that blocks ping packets coming out of the VM - outgoing traffic, you can block specific range of ip (for examples you can block specific IP range. The range that used your host machine).

Since there is only one rule in INPUT chain, I will delete it using `sudo iptables -D INPUT 1` command.

    sudo iptables -A OUTPUT -p icmp --icmp-type echo-request -s 192.168.31.74 -j REJECT
    # -A INPUT -- add rule to INPUT chain
    # -p icmp -- match ICMP protocol
    # --icmp-type echo-request -- match ICMP echo requests
    # -s 192.168.31.74 -- source is 192.168.31.74 (IP address of VM, so outgoing packets will be matched)
    # -j REJECT -- reject matched packets

![](images/2.2.1.png)\
![](images/2.2.2.png)

## 3. Install web server on your VM (if you don't have it). Add a rule to the firewall that prevents loading your web server’s IP from the host machine in the browser. Test it by trying to load web page from the VM in your host machine’s web browser (http://IP_address_of_your_vm).

Initially I have working Web server (nginx) and following html page.\
![](images/3.1.png)

Then I apply the following rule.

![](images/3.2.png)

After that my page is not accessible from my host machine.

![](images/3.3.png)

## 4. Create firewall rules that opens ports for SMTP, DNS, POP3 and SSH connections in your VM and blocking others ports (for you to be able to work with mentioned services).

*Hint*: to test some services you can use netcat utility

Record your rules in the report, its short explanation and results of the implementations (acceptance testings)

I will use the following rules:

    # accept SMTP
    sudo iptables -A INPUT -p tcp --dport 587 -j ACCEPT

    # accept DNS over TCP
    sudo iptables -A INPUT -p tcp --dport 53 -j ACCEPT

    # accept DNS over UDP
    sudo iptables -A INPUT -p udp --dport 53 -j ACCEPT

    # accept non-encrypted POP3
    sudo iptables -A INPUT -p tcp --dport 110 -j ACCEPT  
    
    # accept encrypted POP3
    sudo iptables -A INPUT -p tcp --dport 995 -j ACCEPT

    # accept SSH
    sudo iptables -A INPUT -p tcp --dport 22 -j ACCEPT

    # reject other packets
    sudo iptables -A INPUT -j REJECT

![](images/4.1.png)

So, after scanning my VM I got the following result:\
![](images/4.2.png)

## Bonus. Create a scenario on your machine and implement it where you can apply NAT rules and test it.

### Firstly, Flush all existing rules in iptables.
This is done using `sudo iptables -F` command.

### Add second interface for your VM. Second interface with adapter set to INTERNAL NETWORK and Name=intnet. And assign ip address 192.168.100.5/24 to your interface.
![](images/5.1.png)\
![](images/5.2.png)

### Then Enable IP forwarding on your VM kernel.
This is done using `sudo sysctl -w net.ipv4.ip_forward=1` command.

### Configure the firewall for IP masquerading from internet facing interface.
This is done using `sudo iptables -t nat -A POSTROUTING -o enp0s10 -j MASQUERADE` command.

### You can add second VM with interface which adapter is INTERNAL NETWORK and Name=intnet. Configure static ip address on interface from range 192.168.100.0/24 and set gateway for its interface as 192.168.100.5. Check that you can ping its gateway reachability.
![](images/5.3.png)\
![](images/5.4.png)\
![](images/5.5.png)\
![](images/5.6.png)

### Result: you can reach internet from second VM by pinging [ya.ru](https://ya.ru/).
![](images/5.7.png)

(C) Abuzyar Tazetdinov
