const express = require("express");
const app = express();
const connectDb = require("./src/connection");
const User = require("./src/User.model");

const PORT = 8080;
const HOST = "0.0.0.0";

app.get("/users", async (req, res) => {
  const users = await User.find();

  res.json(users);
});

app.get("/user-create", async (req, res) => {
  const user = new User({ username: "userTest" });

  await user.save().then(() => console.log("User created"));

  res.send("User created \n");
});

app.listen(HOST, PORT, function() {
  console.log(`Listening on ${HOST}:${PORT}`);

  connectDb().then(() => {
    console.log("MongoDb connected");
  });
});
