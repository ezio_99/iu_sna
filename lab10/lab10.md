# Lab10

## Questions

### 1. What user are you logged in as by default?

We can check user using `whoami` command. Usually default user inside of docker container is `root`. Inside of
Dockerfile we can change user using `USER` directive.

### 2. If you start and then exit an interactive container, and then use the `docker run -it ubuntu:xenial /bin/bash` command again, is it the same container? How can you tell?

They are not the same container, because when we run image, new container is created. We can check this
using `docker ps -a` command.\
![](images/2.png)

### 3. Run the image you just built. Since we specified the default CMD, you can just do `docker run -it mypython:latest`. What do you observe?

Used Dockerfile is below:

    FROM ubuntu:bionic
 
    RUN apt-get update && apt-get install -y python3.6 --no-install-recommends
     
    CMD ["/usr/bin/python3.6", "-i"]

![](images/3.png)

The `-i` flag tells docker to keep STDIN open to your container, and the `-t` flag allocates a pseudo-TTY. So, we have interactive Python interpreter, which is run inside of container.

### 4. Write and build a Dockerfile by deploying python application (e.g. web server, and you can find it in GitHub as example). Submit your Dockerfile with this lab?

For this task I will use small Hello-World server, which is located in `python_app` directory.

Used Dockerfile listed below or can be found inside of `python_app` directory.

    FROM snakepacker/python:all as builder

    RUN python3.8 -m venv /venv && /venv/bin/pip3 install -U pip
    
    COPY requirements.txt /app/requirements.txt
    
    RUN /venv/bin/pip3 install -r /app/requirements.txt
    
    FROM snakepacker/python:3.8 as api
    
    COPY --from=builder /venv /venv
    
    COPY . /app
    
    CMD ["/venv/bin/python3", "/app/main.py"]

![](images/4.1.png)\
![](images/4.2.png)

Purpose of this task is to write single Dockerfile, but for efficient work of server we should consider case with several containers:
* workers that process requests
* nginx, which acts as reverse proxy and send incoming requests to workers

### 5. Paste the output of your docker images command after questions 4 and 5.

![](images/5.png)

## Practical part

Your task is to use docker-compose to deploy the Node.js app with the MongoDB database - repository. The suggested order
of tasks:

1. Write a Dockerfile that will allow you to run the Node.js web application.
2. Write a docker-compose.yml file that will glue the Node.js app container with a MongoDB container.

### Solution.
For this task I will use [this server](https://github.com/Atadzhan/express-mongo-lab), which is cloned to `nodejs_app/express-mongo-lab` directory.

#### 1. Paste your Dockerfile for the Node.js web application.

    FROM node:10

    WORKDIR /app
    
    COPY express-mongo-lab/package-lock.json /app/package-lock.json
    COPY express-mongo-lab/package.json /app/package.json
    
    RUN npm install
    
    COPY express-mongo-lab /app/express-mongo-lab
    
    EXPOSE 8080
    
    CMD ["node", "/app/express-mongo-lab/server.js"]

#### 2. Paste your docker-compose.yml file.

    version: '3'

    services:
        node:
            build:
                context: .
                dockerfile: Dockerfile
            container_name: node
            depends_on:
                - mongo
            ports:
                - "8080:8080"
    
        mongo:
            image: mongo
            container_name: mongo
            volumes:
                - mongo:/data/db
    
    volumes:
        mongo:

![](images/7.png)

(C) Abuzyar Tazetdinov
