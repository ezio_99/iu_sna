from flask import Flask

app = Flask(__name__)


@app.route("/")
def hello_world():
    return "Hello, World!\n"


if __name__ == "__main__":
    import os

    app.run(
        host=os.environ.get("FLASK_HOST", "0.0.0.0"),
        port=os.environ.get("FLASK_PORT", "8000"),
        debug=os.environ.get("FLASK_DEBUG", "false").lower() == "true",
    )
