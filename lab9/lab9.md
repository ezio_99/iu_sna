# Lab9

## 1. Configure static web page and generate ssl certificate (certificate with your own domain name) and configure it in the web server, enable redirection from http to https. Show your configuration of nginx and certificate used (make screenshots from the browser).

Before starting this exercise I will off default nginx website removing symlink `/etc/nginx/sites-enabled/default`.

For this exercise I will use `index.html` (can be found in this directory). I will place this file
into `/var/www/home.net/html` directory in my VM.\
![](images/1.1.png)

Also, I will place certificates from previous lab into `/etc/nginx/certs/home.net` directory.\
![](images/1.2.png)

Then I put the following configuration for our website into `/etc/nginx/sites-available/home.net`.

    server {
        listen 80;
        server_name home.net;
    
        if ($host = home.net) {
            return 301 https://$host$request_uri;
        }
    
        return 404;
    }
    
    server {
        listen 443;
        ssl_certificate     /etc/nginx/certs/home.net/home.net.crt;
        ssl_certificate_key /etc/nginx/certs/home.net/home.net.key;
    
        server_name home.net;
    
        access_log /var/log/nginx/home.net.access.log;
        error_log  /var/log/nginx/home.net.error.log;
    
        root /var/www/home.net/html;
        index index.html index.htm;
    
        location / {
            try_files $uri $uri/ =404;
        }
    }

Then we need to enable our website by creating symlink to file `/etc/nginx/sites-availabe/home.net`. We should place
this symlink into `/etc/nginx/sites-enabled` directory. For doing that I will use
command `sudo ln -s /etc/nginx/sites-available/home.net /etc/nginx/sites-enabled/`.

So, time to test our configuration and reload server.\
![](images/1.3.png)

I will add `home.net` to `/etc/hosts` on my host machine for making queries to website.\
![](images/1.4.png)

Now time to access our website. First I will try using HTTP protocol. As expected, our web server give us status 301.\
![](images/1.5.png)

For testing HTTPS protocol I will use web browser.\
![](images/1.6.png)

## 2. On the web server configure maximum file upload 1 GB to site. If you have any other useful configuration, it also could be shown.

Configuring maximum body size can be done using `client_max_body_size 1g;` directive. Only this will be added to previous configuration, so I don't think that showing config again will be useful.

## 3. Configure your machine synchronization with any NTP server which located in the United Kingdom.

I will change `/etc/ntp.conf` as follows:\
![](images/3.1.png)

Restart NTP service using `sudo service ntp restart` command.

As it can be seen, server added in previous steps is present.\
![](images/3.2.png)

(C) Abuzyar Tazetdinov
