# Lab2

## Excercise 3 - Investigating hardware, access control, resources utilization

### 1. Identify how many CPU are in your environment? What is a Linux kernel module?
For doing this I will use `cat /proc/cpuinfo` command.\
![](images/1.png)\
As it can be seen, my virtual machine has 4 cores.

Linux kernel module are piece of code that can be loaded and unloaded into the kernel if needed. They extend the functionality of the kernel without the need to reboot the system.

### 2. Which command will show statistics about your free/used memory? Describe all fields from the output of the command (for example point the difference between free and available)?
For displaying information about memory `free -h` can be applied.\
![](images/2.png)\
Rows description:
1. Mem - Main physical memory.
2. Swap - "Extra" memory on secondary storage.

Columns description:
1. total - Total memory.
2. used - Memory used by processes at moment of call `free -h`. It is calculated by formula `total - free - buffers - cache`
3. free - Memory which is not used yet.
4. shared - Memory used (mostly) by `tmpfs`.
5. buff/cache - Memory used by kernel buffers and page cache and slabs.
6. available - Estimation of how much memory is available for starting new applications, without swapping. Sometimes this value differs from free, because free shows really free memory and available shows estimation taking into account future cleaning caches if necessary.

### 3. If you list the content of directory using for example “$ls -al” what do numbers in the column following after permissions information tell you?
![](images/3.png)\
This number represents the number of links to file.

### 4. What is the sticky bit? Show the file or directory with your configured sticky bit.
For files - tell OS to retain file in main memory.

For directories - only owner (or root) can rename or delete files in this directory.\
![](images/4.png)

### 5. Which command will show the available disk space on the Unix/Linux system?
For displaying information about available (and not only available) disk space
`df -hT` command can be used.\
![](images/5.png)

### 6. How to add a new system user without home directory and login?
Using `sudo useradd -M <username>` command.\
![](images/6.png)

### 7. Explain the differences among the following umask values: 000, 002, 022, 027, 077, and 277.
`umask` is simple, just subtract `umask` value from `777` for directories and `666` for files,
and you will get default permissions for directories and files.

1. umask = 000 - anyone has full access
   1. <pre>directories = 777 = rwxrwxrwx</pre>
   2. <pre>files       = 666 = rw-rw-rw-</pre>
2. umask = 002 - others have no write access
   1. <pre>directories = 775 = rwxrwxr-x</pre>
   2. <pre>files       = 664 = rw-rw-r--</pre>
3. umask = 022 - group and others have no write access
   1. <pre>directories = 755 = rwxr-xr-x</pre>
   2. <pre>files       = 644 = rw-r--r--</pre>
4. umask = 027 - owner have full access, group has no write access, others have no access
   1. <pre>directories = 750 = rwxr-x---</pre>
   2. <pre>files       = 640 = rw-r-----</pre>
5. umask = 077 - only owner has full access
   1. <pre>directories = 700 = rwx------</pre>
   2. <pre>files       = 600 = rw-------</pre>
6. umask = 277 - only owner has read access
   1. <pre>directories = 500 = r-x------</pre>
   2. <pre>files       = 400 = r--------</pre>

### 8. You have already configured swap in the exercise and the next step to increase or resize you swap space x2. Provide steps to take so.
1. Deactivate and delete old swap file.\
![](images/8.1.png)
2. Create new swap file and set permissions.\
![](images/8.2.png)
3. Turn on new swap file.\
![](images/8.3.png)

(C) Abuzyar Tazetdinov
