# Lab5

## IPv4

### 1. You have a range 172.16.200.0/22

#### 1. Identify subnet range

[172.16.200.1; 172.16.203.254]

#### 2. How many usable IP addresses?

1022

#### 3. Identify starting IP and ending IP

Starting IP - 172.16.200.1

Ending IP - 172.16.203.254

### 2. You have a range 10.16.200.12/17

#### 1. Identify subnet range

[10.16.128.1; 10.16.255.254]

#### 2. How many usable IP addresses?

32766

#### 3. Identify starting IP and ending IP

Starting IP - 10.16.128.1

Ending IP - 10.16.255.254

### 3. You have a range 192.168.0.0/24 and divide into small subnets:

1. Subnet with 29 hosts
2. Subnet with 120 hosts
3. Subnet with 60 hosts

Let us consider last octet and start from bigger network and go to smaller.

    0|0000000 -- 192.168.0.0/25 -- 126 hosts -- subnet with 120 hosts
    10|000000 -- 192.168.0.128/26 -- 62 hosts -- subnet with 60 hosts
    110|00000 -- 192.168.0.192/27 -- 30 hosts -- subnet with 29 hosts

## Network management in Ubuntu

### 1. Add several IP addresses to interface using netplan and ping them.

1. Before using netplan I have the following configuration:\
   ![](images/1.png)\
   ![](images/2.png)
2. Edit `/etc/netplan/01-network-manager-all.yaml` as following:\
   ![](images/3.png)
3. Apply changes above:\
   ![](images/4.png)
4. Check current configuration:\
   ![](images/5.png)\
   ![](images/6.png)
5. Try to ping using new addresses:\
   ![](images/7.png)

(C) Abuzyar Tazetdinov
