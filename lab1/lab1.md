# Lab 1

## Excercise 1 - Text processing
1. What is `/etc/apt/sources.list.d` directory stands for? How can you use it? Provide an example.
2. How can you add/delete 3rd party repositories to install required software? Provide an example.
3. When do you need to get public key for the usage of the remote repo? Provide an example.

## Answers
1. `/etc/apt/sources.list.d` contains files which points to repositories with packages. We can add location of repository to file and place it in this directory (make sure that you trust this repository).\
\
Example: installing Sublime Text text editor\
1.1. Download GPG key and add it\
`wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -`\
![](images/1.1.png)\
1.2. Add package to work with https sources\
`sudo apt-get install apt-transport-https`\
![](images/1.2.png)\
1.3. Add Sublime Text's repository to `/etc/apt/sources.list.d/sublime-text.list`\
`echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list`\
![](images/1.3.png)\
1.4. Update meta information about packages, since we add new repository, and we need retrieve info about packages from new repository.\
`sudo apt-get update`\
![](images/1.4.png)\
1.5. Install package from new repository\
`sudo apt-get install sublime-text`\
![](images/1.5.png)


2. For adding/deleting 3rd party repositories we need just edit files in `/etc/apt/sources.list.d/` or in general file `/etc/apt/sources.list`. Example of doing that can be found in `1.3`.


3. Usually public keys can be found by links, which placed together with installation instructions in documentation of package. For downloading public key we can use `wget`. Example of doing that can be found in `1.1`.

## Excercise 2 - Managing processes
1. Describe how `htop` works. Explain all important fields in its output based on your system resources.
2. Explain briefly each process states from the output of the `htop` command.
3. What happens to a child process that dies and has no parent process to wait for it and what’s bad about this?
4. How to know which process ID used by application?
5. What is a zombie process and what could be the cause of it? How to find and kill zombie process?

## Answers
1. `htop` displays current information about system and hardware.\
![](images/htop.png)\
In top of program we can find information about hardware. In left side placed numbered cores of CPU, below cores information about RAM and Swap. Different colors in status bar have special meaning and can be described if you press `F1` key. In right side we can find number of running processes and threads, 3 number about load average for different time intervals (1, 5 and 15 minutes respectively). Uptime means how long time ago system started.\
\
Below we can find table with running processes and threads. Looking to table's header, we can identify responsibility of each column:\
PID - ID of process\
USER - Process owner\
PRI - Priority of process (lower numbers have higher priority)\
NI - Nice value of process (in other words it's priority)\
VIRT - Amount of virtual memory taken by process\
RES - Amount of physical memory taken by process (in kilobytes)\
SHR - Amount of shared memory used by process\
S - Current status of process\
CPU% - Percentage of the processor time used by the process\
MEM% - Percentage of the physical memory used by the process\
TIME+ - How much CPU time used by the process\
Command - Command which started this process


2. There are several states, which process can have.\
R: Running – Process actively using CPU\
T/S: Traced/Stopped – Process currently stopped (paused)\
Z: Zombie or defunct – Process completed execution but still has an entry in the process table\
S: Sleeping – Process sleeps or wait input


3. Child process that dies and has no parent process to wait for it becomes zombie process. They only take place in process table and don't consume other resources. But process table have limited space and high amount of zombies can cause problems if limit of process table will be reached. I think there is very good answer on StackOverflow about zombie and orphan processes, so I decided leave the [link](https://stackoverflow.com/a/20689837) if someone interested.


4. PID of running process can be found using several ways. I will show 2 ways that I'm often using. For demonstrating I will run `sleep infinity` and search his PID. After that I will kill process using `kill <PID>` command, so in each exercise PIDs will be different.\
4.1. Using `ps aux | grep <process_command>` command\
![](images/4.1.png)\
4.2. Using `htop`: open `htop`, press `F3` and start typing process' command. After that you can kill process using `F9` key and sending `SIGTERM` signal.\
![](images/4.2.1.png)\
![](images/4.2.2.png)


5. Zombie process is a process, which exit code is not read (or "reaped") by parent process. I will use simple script for creating zombie process and demonstrating how to kill it.\
5.1. For finding zombie process we can use `ps axo stat,ppid,pid,comm | grep -w defunct` command\
![](images/5.1.png)\
5.2. Send `SIGKILL` to zombie process using `kill -9 <zombie_process_PID>` command.
![](images/5.2.png)

(C) Abuzyar Tazetdinov
