# Lab11

## 1. Create new user and grant them admin rights.

1. Create user using `CREATE USER test_user;` command.
2. Add admin rights to created user using `ALTER USER test_user with SUPERUSER;` command.
3. Check that everything is done as we expect using psql's `\du` command.

![](images/1.png)

## 2. Create new database, switch to new database and create table in your new database.

1. Create new database using `CREATE DATABASE test_db;` command.
2. Connect to created database using psql's `\c test_db;` command.
3. Create table using `CREATE TABLE public.test (id int PRIMARY KEY, name varchar(128) NOT NULL);` command.
4. Show tables using psql's `\dt+` command.
5. Check structure of created table using `SELECT table_name, column_name, data_type FROM information_schema.columns WHERE table_name = 'test';` command.

![](images/2.png)

## 3. Backup your new database by naming it.

1. Backup database using `pg_dump`.
2. Check generated file's head.

![](images/3.png)

## 4. Delete your new database and restore it from backup which you created in 3rd question.

Drop database and try to connect to it and this try will fail.

![](images/4.1.png)

First, we create database with same name, where we will restore dropped database. Then restore database running generated backup script.

![](images/4.2.png)

Now, we can connect to restored database and our table is here.

![](images/4.3.png)

(C) Abuzyar Tazetdinov
