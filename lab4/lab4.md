# Lab4

### 1. Create backup for any directory with files inside. Create cron job which backups directory at the 5th day of every month.

Backup script in `1.bash`. Also content listed below.

    #!/usr/bin/env bash
    
    target=$1
    backup_dir=$2
    
    if [ -z "$1" ]; then
        echo "You should provide path to backup target as first argument"
        exit 1
    fi
    
    if [ -z "$2" ]; then
        echo "You should provide path to directory where to store backups as second argument"
        exit 1
    fi
    
    target_parent_dir="$(dirname "$target")"
    target_basename="$(basename "$target")"
    
    BACKUP_DIRECTORY="${backup_dir}/$(date +"%Y/%m/%d")"
    FILE="$(date +"backup_%H_%M_%S.tar.gz")"
    BACKUP_FILE="${BACKUP_DIRECTORY}/${FILE}"
    mkdir -p "${BACKUP_DIRECTORY}"
    tar cvzf "${BACKUP_FILE}".tar.gz -C "$target_parent_dir" "$target_basename"

Usage:

    ./1.bash <target_to_backup> <directory_to_store_backups>

1. Add cron job.\
   ![](images/1.1.png)
2. Let us check what script works.\
   ![](images/1.2.png)
3. Extract archive and check restored data.\
   ![](images/1.3.png)

### 2. Install nginx and backup directory with location of index.html. Create cron job which backups directory at midnight every Sunday. Also script should delete old or previous backups.

I will skip explanation of how to install nginx, because nginx installed using `apt` and this command already covered in
first lab. nginx stores `index.html` at `/var/www/html`.

For backup, I will use script `2.bash`. Also content listed below.

    #!/usr/bin/env bash

    target=$1
    backup_dir=$2
    is_delete_previous_backups=$3
    
    if [ -z "$1" ]; then
        echo "You should provide path to backup target as first argument"
        exit 1
    fi
    
    if [ -z "$2" ]; then
        echo "You should provide path to directory where to store backups as second argument"
        exit 1
    fi
    
    target_parent_dir="$(dirname "$target")"
    target_basename="$(basename "$target")"
    
    BACKUP_DIRECTORY="${backup_dir}/$(date +"%Y/%m/%d")"
    FILE="$(date +"backup_%H_%M_%S.tar.gz")"
    BACKUP_FILE="${BACKUP_DIRECTORY}/${FILE}"
    
    if [ "$is_delete_previous_backups" = true ]; then
        rm -rf "$backup_dir"
    fi
    
    mkdir -p "${BACKUP_DIRECTORY}"
    tar cvzf "${BACKUP_FILE}".tar.gz -C "$target_parent_dir" "$target_basename"

Usage:

    ./2.bash <target_to_backup> <directory_to_store_backups> [true/false]
    # last flag responsible for deleting previous backups
    # true -- delete
    # false -- keep as it is
    # default value is false

1. Add cron job.\
   ![](images/2.1.png)
2. Let us check what script works.\
   ![](images/2.2.png)
3. Extract archive and check restored data.\
   ![](images/2.3.png)

### Bonus: create a script which check availability of IP address or network interface(ethernet or wlan) and put this script to cron job which runs scripts every 5 minutes.
Script can be found in `3.bash`. Also content listed below.

    #!/usr/bin/env bash

    interface=$1
    address=$2
    delay=10
    
    if [ -z "$1" ]; then
        echo "You should provide interface name as first argument"
        exit 1
    fi
    
    if [ -z "$2" ]; then
        echo "You should provide IP address for interface testing as second argument"
        exit 1
    fi
    
    # keep $interface alive
    echo "................................................"
    date "+%Y.%m.%d %H:%M:%S"
    echo " "
    
    ping -c 4 "$address"
    
    if [ $? == 0 ]; then
        echo "$interface is alive"
    else
        echo "No network connection, restarting $interface"
    
        sudo ifconfig "$interface" down
        sleep "$delay"
        sudo ifconfig "$interface" up
    fi
    echo "................................................"
    echo " "

Usage:
    
    ./3.bash <interface> <ip_address>

1. Add cron job.\
   ![](images/3.1.png)
2. Let us check what script works.\
   ![](images/3.2.png)

(C) Abuzyar Tazetdinov
