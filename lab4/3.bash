#!/usr/bin/env bash

interface=$1
address=$2
delay=10

if [ -z "$1" ]; then
    echo "You should provide interface name as first argument"
    exit 1
fi

if [ -z "$2" ]; then
    echo "You should provide IP address for interface testing as second argument"
    exit 1
fi

# keep $interface alive
echo "................................................"
date "+%Y.%m.%d %H:%M:%S"
echo " "

ping -c 4 "$address"

if [ $? == 0 ]; then
    echo "$interface is alive"
else
    echo "No network connection, restarting $interface"

    sudo ifconfig "$interface" down
    sleep "$delay"
    sudo ifconfig "$interface" up
fi
echo "................................................"
echo " "
