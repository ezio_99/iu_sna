#!/usr/bin/env bash

target=$1
backup_dir=$2

if [ -z "$1" ]; then
    echo "You should provide path to backup target as first argument"
    exit 1
fi

if [ -z "$2" ]; then
    echo "You should provide path to directory where to store backups as second argument"
    exit 1
fi

target_parent_dir="$(dirname "$target")"
target_basename="$(basename "$target")"

BACKUP_DIRECTORY="${backup_dir}/$(date +"%Y/%m/%d")"
FILE="$(date +"backup_%H_%M_%S.tar.gz")"
BACKUP_FILE="${BACKUP_DIRECTORY}/${FILE}"
mkdir -p "${BACKUP_DIRECTORY}"
tar cvzf "${BACKUP_FILE}".tar.gz -C "$target_parent_dir" "$target_basename"
